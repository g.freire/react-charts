/* 
  Renders main page of application, responsible for components composition,
  state management and data bindings
*/

import React, { Component } from "react";
import "./App.css";

//services
import InputParser from "./services/input-parser";
import { mockInputDataChallenge, mockInputData } from "./services/mock-data";
// components
import Header from "./components/layout/header";
import CodeEditor from "./components/code-editor/code-editor";
import Chart from "./components/chart/chart";
import Footer from "./components/layout/footer";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // inputEditor: mockInputData,
      inputEditor: mockInputDataChallenge,
      dataChart: {
        datasets: [],
      },
    };
  }
  
  // lifecycle hook
  componentDidMount() {
    this.preprocessInputEditor(this.inputEditor);
  }

  // calls the preprocessing service
  preprocessInputEditor = () => {
    const inputState = this.state.inputEditor;
    const _InputParser = new InputParser(inputState);
    _InputParser.preprocessData();

    // update the state after preprocessing
    this.setState({
      dataChart: {
        datasets: _InputParser.generateChartData(),
      },
    });
  };

  // calls preprocessing on button click
  buttonClick = () => this.preprocessInputEditor();

  // calls on input change
  onChange = (inputValues) => {
    this.setState({
      inputEditor: inputValues,
    });
  };

  // main page
  render() {
    return (
      <div className="Layout">
        <Header challenger="g-freire Challenge" />
        <CodeEditor values={this.state.inputEditor} onChange={this.onChange} />
        <Chart className="chart" data={this.state.dataChart} />
        <Footer buttonClick={this.buttonClick} />
      </div>
    );
  }
}

export default App;


// TODO
// add draggable component
// validate without span 
// add modal error instead of alerts
// fix timestamp to start at 00:00
// Add material color tuples to constrast bordercolor
// change legend font color
// fix usePointStyle, legend colors buggin after clicking on generate button 
// refactor chart options to external class
// unit tests
// deploy