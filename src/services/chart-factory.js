// creates the chart data structure and returns to main flow

export function chartFactory(parserInstance) {
  
  // util to randomly return some material design hex color 
  function randomMaterialColor() {
    const materialList = [
      "#F1C40F",
      "#E74C3C",
      "#9B59B6",
      "#3498DB",
      "#1ABC9C",
      "#34495E",
      "#2ECC71",
      "#2980B9",
      "#C0392B",
      "#F39C12",
      "#34495E",
      "#27AE60",
    ];
    const indexRandom = Math.floor(Math.random() * 12);
    return materialList[indexRandom];
  }
 
  // creates the chart format data structure
  function generateChartData() {
    for (let key in parserInstance.chartObj) {
      const randomMaterialColorResult = randomMaterialColor();

      const chartData = {
        label: key,
        data: parserInstance.chartObj[key],
        fill: false,
        borderWidth: 2,
        backgroundColor: randomMaterialColorResult,
        borderColor: randomMaterialColorResult,
        pointBorderColor: randomMaterialColorResult,
        pointHoverBackgroundColor: randomMaterialColorResult,
      };
      parserInstance.chartDataCollection.push(chartData);
    }
    return parserInstance.chartDataCollection;
  }
  
  return generateChartData()
}
