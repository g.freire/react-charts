/* 
   Routine to preprocess the input from the code-editor,
   validate the data and creates the chart format data structure
*/

import { validateStream } from './validate' 
import { chartFactory } from './chart-factory' 


class InputParser {
  constructor(data) {
    this.data = data;
    this.group = null;
    this.select = null;
    this.span = {
      begin: null,
      end: null,
    };
    this.chartObj = {};
    this.chartDataCollection = [];
    this.started = false;
  }

  // convert the input using regex, first add quotes to the keys 
  // and then replaces single w/ double quotes
  convertToJsonFormatFromInput(rawData) {
    const inputCollection = rawData.split("\n");
    const parsedResult = [];
    inputCollection.map((line) => {
      parsedResult.push(
        JSON.parse(
          line
            .replace(/([{,])(\s*)([A-Za-z0-9_\-]+?)\s*:/g, '$1"$3":')
            .replace(/'/g, '"')
        )
      );
      return null;
    });
    return parsedResult;
  }

  // calls the validation input routine
  preprocessData() {
    const parsedResult = this.convertToJsonFormatFromInput(this.data);
    validateStream(parsedResult, this);
  }
  
  // calls the routine responsible for creating the chart data structure
  generateChartData(){
    const result = chartFactory(this)
    return result
  }
}
  
export default InputParser;