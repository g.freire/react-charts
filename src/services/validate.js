/* 
   Routine to validate the input according to business
   requirements and map the values to the final chart format
   Constraint: each event has two mandatory fields: timestamp and type.
   All other fields are optional. 
*/

export function validateStream(parsedResult, InputParserInstance) {
  parsedResult.forEach((record) => {

    ///////////////////////////////////////////
    // validating attributes constraint
    ///////////////////////////////////////////
    if ("timestamp" in record && "type" in record) {
      const timestamp = record.timestamp;
      const type = record.type;

      ///////////////////////////////////////////
      // validating START type record
      ///////////////////////////////////////////
      if (type === "start") {
        if (!("group" in record) || record.group.length < 0)
          alert("Ops! Input type start should have at least one group. eg. group: ['os', 'browser'] ");
        if (!("select" in record) || record.select.length < 0) {
          alert("Ops! Input type start should have at least one select. eg. group: ['os', 'browser'] ");
        }
        if (InputParserInstance.started === false) {
          InputParserInstance.started = true;
          InputParserInstance.group = record.group;
          InputParserInstance.select = record.select;
        } else {
          alert("Ops! Input should have only one start type");
        }
      }
      ///////////////////////////////////////////
      // validating and creating SPAN type record
      ///////////////////////////////////////////
      else if (type === "span") {
        if (!InputParserInstance.started) {
          alert("Ops! Input type span before input type start");
        }
        if (record.begin > record.end) {
          alert("Ops! Input type span end attribute should be greater than begin attribute");
        } else {
          InputParserInstance.span.begin = record.begin;
          InputParserInstance.span.end = record.end;
        }
      }

      ///////////////////////////////////////////
      // validating and creating DATA type record
      ///////////////////////////////////////////
      else if (type === "data") {
        if (!InputParserInstance.started) {
          alert("Ops! Input type date before input type start");
        }
        // span bounderies, all data outside this range may be ignored.
        if (
          timestamp >= InputParserInstance.span.begin &&
          timestamp <= InputParserInstance.span.end
        ) {
          // validates / gets the groups
          const properties = [];
          InputParserInstance.group.forEach((x) => {
            if (x in record) {
              properties.push(record[x]);
            } else {
              alert(`"Ops! Input typedata without required group ${x}"`);
            }
          });
          const legendPrefix = properties.join(" ");

          // validates / gets the selects values
          InputParserInstance.select.forEach((x) => {
            if (x in record) {
              //legend
              const axesKey = `${legendPrefix} ${x}`;
              // axes data
              const axesData = {
                x: timestamp - InputParserInstance.span.begin,
                y: record[x],
              };

              // creates the chart data object
              if (axesKey in InputParserInstance.chartObj) {
                InputParserInstance.chartObj[axesKey].push(axesData);
              } else {
                InputParserInstance.chartObj[axesKey] = [axesData];
              }
            }
          });
        }
      }
      ///////////////////////////////////////////
      // validating and creating STOP type record
      ///////////////////////////////////////////
      else if (type === "stop") {
        if (InputParserInstance.started === true) {
          InputParserInstance.started = false;
        } else {
          alert(`"Ops! Input type stop before/without start"`);
        }
      }
    } else {
      alert(`"Ops! Required type and timestamp fields not found"`);
    }
    return InputParserInstance;
  });
}
