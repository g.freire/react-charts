import React from "react";
import "./header.css";

const Header = (props) => (
  <header>
    <div className="header">{props.challenger}</div>
  </header>
);

export default Header;
