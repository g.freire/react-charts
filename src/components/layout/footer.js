/* Page's Footer */

import React from "react";
import Button from "@material-ui/core/Button";
import {  withStyles } from '@material-ui/core/styles';
import "./footer.css";

const ColorButton = withStyles((theme) => ({
	root: {
	  color: theme.palette.getContrastText('#2581dd'),
	  fontWeight: 100,
	  backgroundColor: '#2581dd',
	  '&:hover': {
		backgroundColor: '#2581dd',
	  },
	},
  }))(Button);

const Footer = (props) => (
  <footer className="footer">
    <ColorButton  className="genButton" variant="contained" color="primary" onClick={props.buttonClick}>
      GENERATE CHART
    </ColorButton >
  </footer>
);

export default Footer;
