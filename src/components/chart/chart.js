/* 
   Renders chart.js lib data structure with inline options
*/

import React, { Component } from "react";
import { Line, defaults  } from "react-chartjs-2";
import "./chart.css";

defaults.global.animation = false;
// defaults.global.legend.labels.usePointStyle = true;
defaults.global.legend.labels.fontSize = 16;
defaults.global.legend.labels.fontFamily = 'Source Sans Pro';
// defaults.global.legend.labels.fontFamily = "Sources Code Pro";

class Chart extends Component {
  
  render() {
    return (
      <div className="chartcontainer">
        <article className="canvas-container" >
          
          <Line
            data={ this.props.data }
            options={{
              aspectRatio: 1,
              responsive: true,
              maintainAspectRatio: false,
              title: {
                display: false,
              },
              legend: { 
                display:true,
                position: "right",
                maxHeight: 15,
                labels:{
                  // fontColor: this.props.data.datasets.legendColors,
                  // usePointStyle: true,
                }
              },
              scales: {
                yAxes: [
                  {
                    ticks: {
                      source: "data",
                      beginAtZero: true,
                    },
                  },
                ],
                xAxes: [
                  {
                    type: "time",
                    time: {
                      unit: "minute",
                      displayFormats: {
                        minute: "HH:mm",
                      },
                    },
                    ticks: {
                      source: "data",
                      beginAtZero: true,
                    },
                  },
                ],
              },
            }}
          />
        </article>
      </div>
    );
  }
}

export default Chart;
