import React, { Component } from "react";
import AceEditor from "react-ace";
import "./code-editor.css";
import "brace/mode/java";
import "brace/theme/twilight";

class CodeEditor extends Component {

  // Main code editor area
  render() {
    const { values, onChange } = this.props;
    return (
      <div className="codeEditor" >
        <AceEditor
          mode="javascript"
          theme="twilight"
          onChange={onChange}
          onValidate={null}
          name="code-editor"
          width="100%"
          height="35vh"
          showPrintMargin={false}
          highlightActiveLine={true}
          focus={true}
          wrapEnabled={true}
          value={values}
          fontSize={14}
          editorProps={{ $blockScrolling: false }}
          style={{
            fontFamily: "'Source Code Pro', monospace",
            minHeight: "100px",
          }}
        />
      </div>
    );
  }
}

export default CodeEditor;

// TODO: make draggable area, fix warning